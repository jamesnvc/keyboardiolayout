/*
  NB: to build:
    copy to /home/james/software/kaleidoscope/Kaleidoscope-Bundle-Keyboardio/avr/libraries/Kaleidoscope/examples/Devices/Keyboardio/Model01
    (seems like just symlinking doesn't actually work for some weird reason with their magic makfiles...)
    sudo systemctl stop ModemManager
    make compile flash
 */

#ifndef BUILD_INFORMATION
#define BUILD_INFORMATION "locally built"
#endif

#define DEBUG_SERIAL false

#define MY_EEPROM_SETTINGS_VERSION 0x11

#define WITH_STALKER_LEDS 0
// Personal plugins for testing
#define WITH_PREFIX_LAYER 0
#define WITH_TOIFALONE 0
#define WITH_QUKEYS 0

#define WITH_EEPROM 0

#define WITH_TEST_MODE 0

#include "Kaleidoscope.h"

#if WITH_TEST_MODE
#include "Kaleidoscope-Model01-TestMode.h"
#endif

#include "Kaleidoscope-HostOS.h"
#include "Kaleidoscope-Unicode.h"
#include "Kaleidoscope-MouseKeys.h"
#include "Kaleidoscope-Macros.h"
#include "Kaleidoscope-LEDControl.h"
// #include "Kaleidoscope-SpaceCadet.h"
#include "Kaleidoscope-OneShot.h"
#include "Kaleidoscope-Escape-OneShot.h"
#include "Kaleidoscope-TapDance.h"
#include "Kaleidoscope-Leader.h"
#include "Kaleidoscope-MagicCombo.h"
#include "Kaleidoscope-USB-Quirks.h"

#include "Kaleidoscope-LED-ActiveModColor.h"
#if WITH_STALKER_LEDS
#include "Kaleidoscope-LED-Stalker.h"
#endif
#include "Kaleidoscope-LEDEffect-Breathe.h"
// #include "Kaleidoscope-LEDEffect-BootGreeting.h"
#include "Kaleidoscope-LEDEffect-Chase.h"
#include "Kaleidoscope-LEDEffect-Rainbow.h"
#include "Kaleidoscope-LEDEffect-SolidColor.h"

#include "Kaleidoscope-FocusSerial.h"

// EEPROM stuff isn't working now, after change from Focus to
// FocusSerial library
#if WITH_EEPROM
#include "Kaleidoscope-EEPROM-Settings.h"
#include "Kaleidoscope-EEPROM-Keymap.h"
// #include "Kaleidoscope-Colormap.h"
#endif

#if WITH_QUKEYS
#include "Kaleidoscope-Qukeys.h"
#endif

#if WITH_TOIFALONE
#include "Kaleidoscope-ToIfAlone.h"
#endif

#if WITH_PREFIX_LAYER
#include "Kaleidoscope-PrefixLayer.h"
#endif

/* Macros */

enum {
  // Typing Greek chars
  M_ALPHA,
  M_BETA,
  M_GAMMA,
  M_DELTA,
  M_EPSILON,
  M_LAMBDA,
  M_PII, // M_PI is already defined as a math constant
  M_TAU,
  M_THETA,
  M_OMEGA,
  // Typing Emoji
  M_THUMBSUP,
  M_BICEP,
  M_HAMMER_AND_SICKLE,
  M_LAUGHING,
  M_NAMASTE,
  M_GRIMACE,
  M_UPSIDEDOWN,
  M_GRIN
};

namespace jamesnvc {
namespace UnicodeInput {
bool useNative = false;
}
}

static void UnicodeType(uint32_t letter) {
  if (jamesnvc::UnicodeInput::useNative) {
    Unicode.type(letter);
  } else {
    ::Focus.send(F("unicode_input"), letter, F("\n"));
  }
}


static void greekCharMacro(uint32_t letter, uint8_t keyState) {
  if (!keyToggledOn(keyState)) {
    return;
  }
  bool shifted =
    Kaleidoscope.hid().keyboard().wasModifierKeyActive(Key_LeftShift) ||
    Kaleidoscope.hid().keyboard().wasModifierKeyActive(Key_RightShift);
  UnicodeType(letter - (shifted ? 0x20 : 0));
}

static void macroTypeUnicode(uint32_t codepoint, uint8_t keyState) {
  if (!keyToggledOn(keyState)) {
    return;
  }
  UnicodeType(codepoint);
}

const macro_t * macroAction(uint8_t macroIndex, KeyEvent &event) {
  uint8_t keyState = event.state;
  switch (macroIndex) {
  case M_ALPHA:
    // α Α
    greekCharMacro(0x03b1, keyState);
    break;
  case M_BETA:
    // β Β
    greekCharMacro(0x03b2, keyState);
    break;
  case M_GAMMA:
    // γ Γ
    greekCharMacro(0x03b3, keyState);
    break;
  case M_DELTA:
    // δ Δ
    greekCharMacro(0x03b3, keyState);
    break;
  case M_EPSILON:
    // ε Ε
    greekCharMacro(0x03b5, keyState);
    break;
  case M_LAMBDA:
    // λ
    greekCharMacro(0x03bb, keyState);
    break;
  case M_PII:
    // π Π
    greekCharMacro(0x03c0, keyState);
    break;
  case M_TAU:
    // τ Τ
    greekCharMacro(0x03c4, keyState);
    break;
  case M_THETA:
    // θ Θ
    greekCharMacro(0x03b8, keyState);
    break;
  case M_OMEGA:
    // ω Ω
    greekCharMacro(0x03c9, keyState);
    break;
  case M_THUMBSUP:
    // 👍
    macroTypeUnicode(0x1f44d, keyState);
    break;
  case M_BICEP:
    // 💪
    macroTypeUnicode(0x1f4aa, keyState);
    break;
  case M_HAMMER_AND_SICKLE:
    // ☭
    macroTypeUnicode(0x262d, keyState);
    break;
  case M_LAUGHING:
    // 😆
    macroTypeUnicode(0x1f606, keyState);
    break;
  case M_NAMASTE:
    // 🙏
    macroTypeUnicode(0x1f64f, keyState);
    break;
  case M_GRIMACE:
    // 😬
    macroTypeUnicode(0x1f62c, keyState);
    break;
  case M_GRIN:
    // 😀
    macroTypeUnicode(0x1f600, keyState);
    break;
  case M_UPSIDEDOWN:
    // 🙃
    macroTypeUnicode(0x1f643, keyState);
    break;
  }
  return MACRO_NONE;
}

/* TapDancing */
namespace jamesnvc {
namespace TapDance {
bool cancelOneShot;
}
}

enum {
  TD_HOMEEND,
  TD_MENU
};

void tapDanceAction(uint8_t tapDanceIndex, byte row, byte col, uint8_t tapCount,
                    kaleidoscope::plugin::TapDance::ActionType tapDanceAction) {
  if (tapDanceAction == kaleidoscope::plugin::TapDance::Release) {
    jamesnvc::TapDance::cancelOneShot = true;
  }

  switch (tapDanceIndex) {
  case TD_MENU:
    // Prog key, so we can press it once for Menu or twice & hold for nothing
    if (HostOS.os() == kaleidoscope::hostos::OSX) {
      return tapDanceActionKeys(tapCount, tapDanceAction, LALT(Key_X), Key_NoKey);
    } else {
      return tapDanceActionKeys(tapCount, tapDanceAction, Key_PcApplication, Key_NoKey);
    }
  case TD_HOMEEND:
    return tapDanceActionKeys(tapCount, tapDanceAction, Key_Home, Key_End);
  }
}

// Layers
enum {
  _DVORAK,
  _QWERTY,
  _NAV,
  _NUMPAD,
#if WITH_PREFIX_LAYER
  _TMUX,
#endif

  LAYER_MAX
};

/* Magic Combos */

#if WITH_EEPROM
void toggleEEPROMKeys(uint8_t combo_index) {
  if (Layer.getKey == EEPROMKeymap.getKeyOverride) {
    Layer.getKey = Layer.getKeyFromPROGMEM;
  } else {
    Layer.getKey = EEPROMKeymap.getKeyOverride;
  }
}
#endif

void toggleKeyboardProtocol(uint8_t combo_index) {
  USBQuirks.toggleKeyboardProtocol();
}

USE_MAGIC_COMBOS(
#if WITH_EEPROM
                 {
                   .action = toggleEEPROMKeys,
                   // prog + led + any + num
                   // (top corners on both halfs)
                   .keys = { R0C0, R0C6, R0C9, R0C15 }
                 },
#endif
                 {
                   .action = toggleKeyboardProtocol,
                   // prog + led + pg dn + esc
                   // (four left corners)
                   .keys = { R0C0, R0C6, R3C0, R2C6 }
                 }
                 );


/* Leader sequences */

static void leaderSwitchOSLinux(uint8_t seq_index) {
  HostOS.os(kaleidoscope::hostos::LINUX);
}

static void leaderSwitchOSMacOS(uint8_t seq_index) {
  HostOS.os(kaleidoscope::hostos::OSX);
}

static void leaderSwitchOSWindows(uint8_t seq_index) {
  HostOS.os(kaleidoscope::hostos::WINDOWS);
}

static void leaderSwitchQwerty(uint8_t seq_index) {
  Layer.activate(_QWERTY);
}

static void leaderSwitchDvorak(uint8_t seq_index) {
  Layer.deactivate(_QWERTY);
  Layer.activate(_DVORAK);
}

static void nextLEDEffect(uint8_t seqIndex) {
  LEDControl.next_mode();
}

static void leaderTypeAGrave(uint8_t seq_index) { UnicodeType(0xe0); }
static void leaderTypeAAcute(uint8_t seq_index) { UnicodeType(0xe1); }
static void leaderTypeACircu(uint8_t seq_index) { UnicodeType(0xe2); }
static void leaderTypeATilde(uint8_t seq_index) { UnicodeType(0xe3); }

static void leaderTypeEGrave(uint8_t seq_index) { UnicodeType(0xe8); }
static void leaderTypeEAcute(uint8_t seq_index) { UnicodeType(0xe9); }
static void leaderTypeECircu(uint8_t seq_index) { UnicodeType(0xea); }

static void leaderTypeOGrave(uint8_t seq_index) { UnicodeType(0xf2); }
static void leaderTypeOAcute(uint8_t seq_index) { UnicodeType(0xf3); }
static void leaderTypeOCircu(uint8_t seq_index) { UnicodeType(0xf4); }
static void leaderTypeOTilde(uint8_t seq_index) { UnicodeType(0xf5); }

static void leaderTypeIAcute(uint8_t seq_index) { UnicodeType(0xed); }

static void leaderTypeCCedilia(uint8_t seq_index) { UnicodeType(0xe7); }

static void leaderTypeUAcute(uint8_t seq_index) { UnicodeType(0xfa); }

// ♔ ♕ ♖ ♗ ♘ ♙
static void leaderTypeChessKing(uint8_t seq_index) { UnicodeType(0x2654); }
static void leaderTypeChessQueen(uint8_t seq_index) { UnicodeType(0x2655); }
static void leaderTypeChessRook(uint8_t seq_index) { UnicodeType(0x2656); }
static void leaderTypeChessBishop(uint8_t seq_index) { UnicodeType(0x2657); }
static void leaderTypeChessKnight(uint8_t seq_index) { UnicodeType(0x2658); }
static void leaderTypeChessPawn(uint8_t seq_index) { UnicodeType(0x2659); }

static void leaderToggleUnicodeInputMethod(uint8_t seq_index) {
  jamesnvc::UnicodeInput::useNative = !jamesnvc::UnicodeInput::useNative;
}

static const kaleidoscope::plugin::Leader::dictionary_t leader_dict[] PROGMEM =
  LEADER_DICT(// switching OS - leader O
              { LEADER_SEQ(LEAD(0), Key_O, Key_L), leaderSwitchOSLinux },
              { LEADER_SEQ(LEAD(0), Key_O, Key_M), leaderSwitchOSMacOS },
              { LEADER_SEQ(LEAD(0), Key_O, Key_W), leaderSwitchOSWindows },
              // switching layout - leader L
              { LEADER_SEQ(LEAD(0), Key_L, Key_Q), leaderSwitchQwerty },
              { LEADER_SEQ(LEAD(0), Key_L, Key_D), leaderSwitchDvorak },
              // switch led modes by double-tapping leader
              { LEADER_SEQ(LEAD(0), LEAD(0)), nextLEDEffect },
              // typing accented characters with leader + accent + char
              { LEADER_SEQ(LEAD(0), Key_Quote, Key_A), leaderTypeAAcute },
              { LEADER_SEQ(LEAD(0), Key_Backtick, Key_A), leaderTypeAGrave },
              { LEADER_SEQ(LEAD(0), Key_6, Key_A), leaderTypeACircu },
              { LEADER_SEQ(LEAD(0), Key_Minus, Key_A), leaderTypeATilde },

              { LEADER_SEQ(LEAD(0), Key_Quote, Key_E), leaderTypeEAcute },
              { LEADER_SEQ(LEAD(0), Key_Backtick, Key_E), leaderTypeEGrave },
              { LEADER_SEQ(LEAD(0), Key_6, Key_E), leaderTypeECircu },

              { LEADER_SEQ(LEAD(0), Key_Quote, Key_O), leaderTypeOAcute },
              { LEADER_SEQ(LEAD(0), Key_Backtick, Key_O), leaderTypeOGrave },
              { LEADER_SEQ(LEAD(0), Key_6, Key_O), leaderTypeOCircu },
              { LEADER_SEQ(LEAD(0), Key_Minus, Key_O), leaderTypeOTilde },

              { LEADER_SEQ(LEAD(0), Key_Quote, Key_I), leaderTypeIAcute },
              { LEADER_SEQ(LEAD(0), Key_Comma, Key_C), leaderTypeCCedilia },
              { LEADER_SEQ(LEAD(0), Key_Quote, Key_U), leaderTypeUAcute },

              { LEADER_SEQ(LEAD(0), Key_C, Key_K), leaderTypeChessKing },
              { LEADER_SEQ(LEAD(0), Key_C, Key_Q), leaderTypeChessQueen },
              { LEADER_SEQ(LEAD(0), Key_C, Key_R), leaderTypeChessRook },
              { LEADER_SEQ(LEAD(0), Key_C, Key_B), leaderTypeChessBishop },
              { LEADER_SEQ(LEAD(0), Key_C, Key_N), leaderTypeChessKnight },
              { LEADER_SEQ(LEAD(0), Key_C, Key_P), leaderTypeChessPawn },

              { LEADER_SEQ(LEAD(0), Key_U), leaderToggleUnicodeInputMethod }
              );

/* Keymaps */

// NoKey (XXX) does nothing, Transparent (___) means fall through to the next active layer
KEYMAPS(
  [_DVORAK] = KEYMAP_STACKED
  (
    Key_PcApplication    , Key_1         , Key_2       , Key_3         , Key_4 , Key_5 , Key_LEDEffectNext ,
    Key_Backtick   , Key_Quote     , Key_Comma   , Key_Period    , Key_P , Key_Y , Key_Tab           ,
    TD(TD_HOMEEND) , Key_A         , Key_O       , Key_E         , Key_U , Key_I ,
    Key_Pipe       , Key_Semicolon , Key_Q       , Key_J         , Key_K , Key_X , OSM(LeftShift)    ,
      OSM(LeftControl) , Key_Backspace , OSM(LeftGui) , Key_LeftParen ,
        OSL(_NAV),

#if WITH_PREFIX_LAYER
    OSL(_TMUX)    , Key_6         , Key_7        , Key_8            , Key_9         , Key_0     , Key_Backslash     ,
#else
    LEAD(0)       , Key_6         , Key_7        , Key_8            , Key_9         , Key_0     , Key_Backslash     ,
#endif
    Key_Enter     , Key_F         , Key_G        , Key_C            , Key_R         , Key_L     , Key_Slash         ,
                    Key_D         , Key_H        , Key_T            , Key_N         , Key_S     , Key_Minus         ,
    OSM(LeftAlt)  , Key_B         , Key_M        , Key_W            , Key_V         , Key_Z     , Key_Equals        ,
      Key_RightParen , Key_Escape , Key_Spacebar , OSM(RightControl) ,
            OSL(_NUMPAD)
  ),

  [_QWERTY] = KEYMAP_STACKED
  (
    LEAD(0),          Key_1, Key_2, Key_3, Key_4, Key_5, Key_LEDEffectNext,
    Key_Backtick, Key_Q, Key_W, Key_E, Key_R, Key_T, Key_Tab,
    Key_PageUp,   Key_A, Key_S, Key_D, Key_F, Key_G,
    Key_PageDown, Key_Z, Key_X, Key_C, Key_V, Key_B, Key_Escape,
        Key_LeftControl, Key_Backspace, Key_LeftGui, Key_LeftShift,
            ShiftToLayer(_NAV),

    LEAD(0),  Key_6, Key_7, Key_8,     Key_9,         Key_0,         LockLayer(_NUMPAD),
    Key_Enter,     Key_Y, Key_U, Key_I,     Key_O,         Key_P,         Key_Equals,
                    Key_H, Key_J, Key_K,     Key_L,         Key_Semicolon, Key_Quote,
    Key_RightAlt,  Key_N, Key_M, Key_Comma, Key_Period,    Key_Slash,     Key_Minus,
        Key_RightShift, Key_LeftAlt, Key_Spacebar, Key_RightControl,
            ShiftToLayer(_NAV)
   ),

  [_NAV] = KEYMAP_STACKED
  (
    ___      , Key_F1            , Key_F2         , Key_F3            , Key_F4          , Key_F5           , ___              ,
    ___      , Key_mouseScrollUp , Key_mouseUp    , Key_mouseScrollDn , Key_mouseWarpNW , Key_mouseWarpNE  , Key_mouseWarpEnd ,
    ___      , Key_mouseL        , Key_mouseDn    , Key_mouseR        , Key_mouseWarpSW , Key_mouseWarpSE  ,
    ___      , Key_mouseScrollL  , Key_Insert     , Key_mouseScrollR  , ___             , ___              , ___              ,
      Key_mouseBtnL , Key_mouseBtnM , Key_mouseBtnR , Key_LeftBracket ,
        ___ ,

    ___      , Key_F6             , Key_F7        , Key_F8            , Key_F9          , Key_F10           , Key_F11         ,
    ___      , ___                , Key_Home      , Key_UpArrow       , Key_End         , ___               , Key_F12         ,
               ___                , Key_LeftArrow , Key_DownArrow     , Key_RightArrow  , ___               , ___             ,
    ___      , Consumer_Mute      , Key_PageUp    , ___               , Key_PageDown    , ___               , ___             ,
      Key_RightBracket , Consumer_Mute , Consumer_VolumeDecrement , Consumer_VolumeIncrement ,
        ___
  ),

  [_NUMPAD] = KEYMAP_STACKED
  (
    ___ , ___          , ___       , ___                   , ___       , ___         , Key_Insert,
    ___ , ___ , ___, ___        , M(M_DELTA), M(M_EPSILON), ___,
    ___ , M(M_GRIN), M(M_LAUGHING) , M(M_GRIMACE), M(M_UPSIDEDOWN), M(M_NAMASTE),
    ___ , M(M_THUMBSUP), M(M_BICEP), M(M_HAMMER_AND_SICKLE), ___       , ___         , ___,
      ___, ___, ___, Key_LeftBracket,
         ___,

    Key_PrintScreen,  ___, Key_Keypad7, Key_Keypad8  ,   Key_Keypad9     , Key_KeypadSubtract , ___      ,
    ___            ,  ___, Key_Keypad4, Key_Keypad5,   Key_Keypad6     , Key_KeypadAdd      , ___      ,
                      ___, Key_Keypad1, Key_Keypad2  ,   Key_Keypad3     , Key_KeypadEquals   , Key_Quote,
    ___            , ___,  Key_Keypad0, Key_KeypadDot, Key_KeypadMultiply, Key_KeypadDivide   , Key_Enter,
      Key_RightBracket, ___, ___, ___,
        ___
  ),

#if WITH_PREFIX_LAYER
  [_TMUX] = KEYMAP_STACKED
  (
    ___, ___, ___, ___, ___, ___, ___,
    ___, ___, ___, ___, ___, ___, ___,
    ___, ___, ___, ___, ___, ___,
    ___, ___, ___, ___, ___, ___, ___,
      Key_LeftArrow, Key_DownArrow, Key_UpArrow, Key_RightArrow,
         ___,

    ___,  ___, ___, ___, ___, ___, ___,
    ___,  ___, ___, ___, ___, ___, ___,
          ___, ___, ___, ___, ___, ___,
    ___, ___,  ___, ___, ___, ___, ___,
      ___, ___, ___, ___,
        ___
  ),
#endif

);

static kaleidoscope::plugin::LEDSolidColor solidIndigo(0, 0, 170);
static kaleidoscope::plugin::LEDSolidColor solidViolet(130, 0, 120);


namespace kaleidoscope {
namespace plugin {
class TransPrideLEDEffect : public LEDMode {
  public:
  TransPrideLEDEffect(void) {}

  void update(void) final {
    uint8_t led_count = Kaleidoscope.device().led_count;
    for (uint8_t i = 0; i < led_count; i++) {
      if (i <= led_count / 3) {
        ::LEDControl.setCrgbAt(i, blue);
        ::LEDControl.setCrgbAt(i*2, blue);
      } else if (i <= 2 * led_count / 3) {
        ::LEDControl.setCrgbAt(i, white);
        ::LEDControl.setCrgbAt(i*2, white);
      } else {
        ::LEDControl.setCrgbAt(i, pink);
        ::LEDControl.setCrgbAt(i*2, pink);
      }
    }
  }

  private:
  cRGB blue = CRGB(85, 205, 252);
  cRGB pink = CRGB(247, 168, 184);
  cRGB white = CRGB(185, 185, 185);
};
}
}

kaleidoscope::plugin::TransPrideLEDEffect TransPrideLEDEffect;

#if WITH_PREFIX_LAYER
static const kaleidoscope::plugin::PrefixLayer::dict_t prefixlayerdict[] PROGMEM =
  PREFIX_DICT({_TMUX, PREFIX_SEQ(LCTRL(Key_B))});
#endif

KALEIDOSCOPE_INIT_PLUGINS(Leader,
#if WITH_TEST_MODE
                          TestMode,
#endif
                          LEDControl, LEDOff,
                          LEDRainbowWaveEffect,
                          solidIndigo, solidViolet,
                          LEDBreatheEffect,
                          TransPrideLEDEffect,
#if WITH_STALKER_LEDS
                          StalkerEffect,
#endif

#if WITH_QUKEYS
                          Qukeys,
#endif

                          TapDance,
                          OneShot, EscapeOneShot,
                          HostOS,
                          Unicode,
                          Macros,
                          MouseKeys,

#if WITH_TOIFALONE
                          ToIfAlone,
#endif

#if WITH_PREFIX_LAYER
                          PrefixLayer,
#endif

#if WITH_EEPROM
                   // eeprom setting stuff
                          EEPROMSettings,
                          EEPROMKeymap,
                          // ColormapEffect,
                          Focus,
                          FocusSettingsCommand,
                          FocusEEPROMCommand,
#endif
                          MagicCombo,
                          USBQuirks,
                          ActiveModColorEffect
                          );

void setup() {
  Kaleidoscope.setup();

#if WITH_EEPROM
  EEPROMKeymap.setup(5, EEPROMKeymap.Mode::EXTEND);
#endif

  MouseKeys.speed = 8;
  MouseKeys.accelSpeed = 0;

#if WITH_TOIFALONE
  static kaleidoscope::ToIfAlone::KeyBinding toifalonemap[] = {
    {Key_Escape, _NUMPAD},
    TOIFALONE_MAP_END
  };
  ToIfAlone.map = toifalonemap;
#endif

#if WITH_PREFIX_LAYER
  PrefixLayer.dict = prefixlayerdict;
#endif

#if WITH_QUKEYS
  QUKEYS(
         kaleidoscope::plugin::Qukey(0, 2, 3, Key_LeftShift)  // E/shift
  );
  Qukeys.qukeys_ = qukeys;
  Qukeys.qukeys_count_ = sizeof(qukeys) / sizeof(kaleidoscope::Qukey);
#endif

  Leader.dictionary = leader_dict;

  LEDRainbowEffect.brightness(150);
  LEDRainbowWaveEffect.brightness(150);

  ActiveModColorEffect.setHighlightColor(CRGB(191, 149, 242));
  ActiveModColorEffect.setStickyColor(CRGB(245, 116, 186));

#if WITH_STALKER_LEDS
  StalkerEffect.variant = STALKER(BlazingTrail);
#endif

  // NumLock.numPadLayer = _NUMPAD;

  HostOS.os(kaleidoscope::hostos::LINUX);

  LEDOff.activate();
}


void loop() {
  Kaleidoscope.loop();

  if (jamesnvc::TapDance::cancelOneShot) {
    OneShot.cancel();
    jamesnvc::TapDance::cancelOneShot = false;
  }
}
