# Keyboardio Model 01 Layout

This repo contains the Arduino IDE sketch for my customized [Model 01](https://shop.keyboard.io/) layout.

It's a Dvorak layout, heavily inspired by [algernon's layout](https://github.com/algernon/Model01-sketch)

[![Base layout](./docs/keyboard-layout.png)](http://www.keyboard-layout-editor.com/#/gists/090a670b2d39625f7b6dd6dadf4bdd8b)

Some plugins whose usage affects the above layout:

  - The Shift, Alt, Control, Shift, and GUI keys are [OneShot][] modifiers.
  - The palm layer-shift keys are OneShot layers.
  - If EEPROM is enabled, there's a [MagicCombo][] set up to switch to using an EEPROM-defined keymap (e.g. set up using [Chrysalis][]) by pressing the top "corner" keys (F21, LED, Leader, and \ in the above image)
  - There's a [MagicCombo][] set up to switch to toggle "boot protocol" (which may be needed for BIOS passwords) by pressing the four corners on the left side.
  - [Leader][] key, followed by `O` and `L` to switch HostOS to Linux (for Unicode typing purposes)
  - Leader key, followed by `O` and `M` to switch HostOS to macOS (for Unicode typing purposes)
  - Leader key, `L`, then `Q` or `D` to switch to QWERTY or Dvorak layout, respectively
  - [TapDance][] used to make the Prog key be Menu on first tap and nothing on second (for flashing)
  - TapDance used to make `Home` act as `End` when double-tapped
  - The parenthesis keys on the thumbs have square brackets on them on both the Nav & Num layers

  [OneShot]: https://github.com/keyboardio/Kaleidoscope-OneShot
  [SpaceCadet]: https://github.com/keyboardio/Kaleidoscope-SpaceCadet
  [MagicCombo]: https://github.com/keyboardio/Kaleidoscope-MagicCombo
  [Leader]: https://github.com/keyboardio/Kaleidoscope-Leader
  [TapDance]: https://github.com/keyboardio/Kaleidoscope-TapDance
  [Chrysalis]: https://github.com/Lepidopterarium/Chrysalis
